# Running Drupal project with this codebase and configuration

1. Clone the repository.
1. Import the database.sql.gz mysql dump.
1. Change $config_directories['sync'] in sites/default/settings.php to "$config_directories['sync'] = 'config/1WJjzwVfClqbzh5kjHxVbeGz-ao7ycduG_J3pgoPbBeUCAnBLUuWtoJUxsmkKs3Aa17wFEwcgg/sync';".
1. Done! You have the same site running.
